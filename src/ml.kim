project private namespace ml
	using lonsdorf.concepts,
	lonsdorf.dd;

observe earth:Region named randomregion
	over space(
			shape="EPSG:4326 POLYGON ((-8.213287402960201 6.877929531446142, -8.264199980404548 6.877929531446142, -8.264199980404548 6.930857016454908, -8.213287402960201 6.930857016454908, -8.213287402960201 6.877929531446142))",
			grid ="1000 m",
			projection = "EPSG:4326"),
	 	time (
	 		start=2016, end=2020); //, step=1.year

observe earth:Region named observ 
	over space(
			urn = "local:angel.gimenez:observ.sandbox:features_buff_onecentdegree",
			grid = "500 m",
			projection = "EPSG:4326"),
	 	time (
	 		start=2015, end=2020); //, step=1.year
	 		
observe earth:Region named eur
	over space(
			shape = "EPSG:4326 POLYGON((3.35 50.50, 3.35 53.50, 6.90 53.50, 6.90 50.50, 3.35 50.50))",
			grid = "1000 m",
			projection = "EPSG:4326"),
	 	time (
	 		start=2007, end=2020); //, step=1.year

 

event PollinatorVisit;

quantity SoilDensity; // Note: wrongly declared as mass in order to give it units when annotated (I don't know how to use the concept 'density' instead of 'mass')
quantity SoilOrganicCarbonContent; 

temperature TemperatureDiurnalRange;
temperature TemperatureWarmestMonth;
temperature TemperatureWettestQuarter;

volume PrecipitationVolumeDriestMonth; 

abstract attribute Biome
	describes im:Type of earth:EarthCover within earth:Location
	has disjoint children
		TropicalAndSubtropicalMoistBroadleafForest        ,
		TropicalAndSubtropicalDryBroadleafForest          ,
		TropicalAndSubtropicalConiferousForest            ,
		TemperateBroadleafAndMixedForest                  ,
		TemperateConiferForest                            ,
		BorealForest                                      ,
		TropicalAndSubtropicalGrasslandSavannaAndShrubland,
		TemperateGrasslandSavannaAndShrubland             ,
		FloodedGrasslandAndSavanna                        ,
		MontaneGrasslandAndShrubland                      ,
		Tundra                                            ,
		MediterraneanForestWoodlandAndScrub               ,
		DesertsAndXericShrubland                          ,
		Mangrove                                          ;
		
class BiomeType
	is type of Biome;	
	
model 'local:angel.gimenez:observ.sandbox:observ_ml.resolve_biomes_rasterized_01deg_1km' as BiomeType
	classified into 
		TropicalAndSubtropicalMoistBroadleafForest         if 1,
		TropicalAndSubtropicalDryBroadleafForest           if 2,
		TropicalAndSubtropicalConiferousForest             if 3,
		TemperateBroadleafAndMixedForest                   if 4,
		TemperateConiferForest                             if 5,
		BorealForest                                       if 6,
		TropicalAndSubtropicalGrasslandSavannaAndShrubland if 7,
		TemperateGrasslandSavannaAndShrubland              if 8,
		FloodedGrasslandAndSavanna                         if 9,
		MontaneGrasslandAndShrubland                       if 10,
		Tundra                                             if 11,
		MediterraneanForestWoodlandAndScrub                if 12,
		DesertsAndXericShrubland                           if 13,
		Mangrove                                           if 14;


// semantically it would be better to have 'model presence of BIOME earth:Region...', 
// but I used 'value of ...' to have numeric values instead of boolean
model value of TropicalAndSubtropicalMoistBroadleafForest
	observing BiomeType named biometype
	set to [ (biometype is ml:TropicalAndSubtropicalMoistBroadleafForest) ? 1:0 ];

model value of TropicalAndSubtropicalDryBroadleafForest
	observing BiomeType named biometype
	set to [ (biometype is ml:TropicalAndSubtropicalDryBroadleafForest) ? 1:0 ];
	
model value of TemperateBroadleafAndMixedForest
	observing BiomeType named biometype
	set to [ (biometype is ml:TemperateBroadleafAndMixedForest) ? 1 : 0 ];
	
model value of TemperateConiferForest
	observing BiomeType named biometype
	set to [ (biometype is ml:TemperateConiferForest) ? 1:0 ];
	
model value of BorealForest
	observing BiomeType named biometype
	set to [ (biometype is ml:BorealForest) ? 1:0 ];
	
model value of TropicalAndSubtropicalGrasslandSavannaAndShrubland
	observing BiomeType named biometype
	set to [ (biometype is ml:TropicalAndSubtropicalGrasslandSavannaAndShrubland) ? 1:0 ];

model value of TemperateGrasslandSavannaAndShrubland
	observing BiomeType named biometype
	set to [ (biometype is ml:TemperateGrasslandSavannaAndShrubland) ? 1:0 ];

model value of MontaneGrasslandAndShrubland
	observing BiomeType named biometype
	set to [ (biometype is ml:MontaneGrasslandAndShrubland) ? 1:0 ];
	
model value of MediterraneanForestWoodlandAndScrub
	observing BiomeType named biometype
	set to [ (biometype is ml:MediterraneanForestWoodlandAndScrub) ? 1:0 ];

model 'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2015',
	'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2016',
	'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2017',
	'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2018',
	'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2019',
	'local:angel.gimenez:observ.sandbox:observ_ml.mean_terraclimate_pdsi_2020' 
	as soil.incubation:DroughtSeverityIndex;

model 'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2015_base_bare_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2016_conso_bare_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2017_conso_bare_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2018_conso_bare_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2019_nrt_bare_coverfraction_layer_epsg_4326'
	as percentage of landcover:BareArea;

model 'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2015_base_builtup_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2016_conso_builtup_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2017_conso_builtup_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2018_conso_builtup_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2019_nrt_builtup_coverfraction_layer_epsg_4326'
	as percentage of landcover:ArtificialSurface; 	
	
model 'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2015_base_shrub_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2016_conso_shrub_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2017_conso_shrub_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2018_conso_shrub_coverfraction_layer_epsg_4326',
	'local:angel.gimenez:observ.sandbox:observ_ml.probav_lc100_global_v3_0_1_2019_nrt_shrub_coverfraction_layer_epsg_4326'
	as percentage of landcover:Shrubland;	

@intensive(space)
model 'local:angel.gimenez:observ.sandbox:sol_bulkdens_fineearth_usda_4a1h_m_250m_b10_10cm_1950_2017_v02'
	as SoilDensity;      	  

@intensive(space) 
model 'local:angel.gimenez:observ.sandbox:sol_organic_carbon_usda_6a1c_m_250m_b10_10cm_1950_2017_v02'
	as SoilOrganicCarbonContent;        

model 'local:angel.gimenez:observ.sandbox:observ_ml.wc2_1_30s_bio_1' as im:Mean earth:AtmosphericTemperature in Celsius;

model 'local:angel.gimenez:observ.sandbox:observ_ml.wc2_1_30s_bio_2' as im:Mean TemperatureDiurnalRange in Celsius;

model 'local:angel.gimenez:observ.sandbox:observ_ml.wc2_1_30s_bio_5' as im:Maximum TemperatureWarmestMonth in Celsius;

model 'local:angel.gimenez:observ.sandbox:observ_ml.wc2_1_30s_bio_8' as im:Mean TemperatureWettestQuarter in Celsius;

model 'local:angel.gimenez:observ.sandbox:observ_ml.wc2_1_30s_bio_14' as PrecipitationVolumeDriestMonth in mm;          	

model each local:angel.gimenez:observ.sandbox:features 
	as earth:Site with occurrence of PollinatorVisit,
		log_visit_ as occurrence of PollinatorVisit,
		annmeant  as im:Mean earth:AtmosphericTemperature in Celsius,
		diurange  as im:Mean TemperatureDiurnalRange in Celsius,
		maxtwarm  as im:Maximum TemperatureWarmestMonth in Celsius,
		twetquart as im:Mean TemperatureWettestQuarter in Celsius,
		precdrmo  as PrecipitationVolumeDriestMonth in mm,
		elev      as geography:Elevation in m,
		pdsi      as soil.incubation:DroughtSeverityIndex,
		bare      as percentage of landcover:BareArea,
		urban     as percentage of landcover:ArtificialSurface,
		shrub     as percentage of landcover:Shrubland,
		soilden   as SoilDensity,
		soilc     as SoilOrganicCarbonContent,
		biome1    as value of TropicalAndSubtropicalMoistBroadleafForest,
		biome2    as value of TropicalAndSubtropicalDryBroadleafForest,
		biome4    as value of TemperateBroadleafAndMixedForest,
		biome5    as value of TemperateConiferForest,
		biome6    as value of BorealForest,
		biome7    as value of TropicalAndSubtropicalGrasslandSavannaAndShrubland,
		biome8    as value of TemperateGrasslandSavannaAndShrubland,
		biome10   as value of MontaneGrasslandAndShrubland,
		biome12   as value of MediterraneanForestWoodlandAndScrub
; 

///////////////////////
// BAYESIAN NETWORK
@intensive(space)
learn occurrence of PollinatorVisit within earth:Site
	observing 
		@archetype earth:Site with occurrence of PollinatorVisit,
		@predictor im:Mean earth:AtmosphericTemperature in Celsius named annmeant ,
		@predictor im:Mean TemperatureDiurnalRange in Celsius      named diurange ,
		@predictor im:Maximum TemperatureWarmestMonth in Celsius   named maxtwarm ,
		@predictor im:Mean TemperatureWettestQuarter in Celsius    named twetquart,
		@predictor PrecipitationVolumeDriestMonth in mm            named precdrmo ,
		@predictor(discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true)) geography:Elevation in m                        named elev     ,
		@predictor soil.incubation:DroughtSeverityIndex            named pdsi     ,
		@predictor(discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true)) percentage of landcover:BareArea                named bare     ,
		@predictor(discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true)) percentage of landcover:ArtificialSurface       named urban    ,
		@predictor(discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true)) percentage of landcover:Shrubland               named shrub    ,
		@predictor SoilDensity                          named soilden  //,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true)) SoilOrganicCarbonContent             named soilc,    
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TropicalAndSubtropicalMoistBroadleafForest         named biome1 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TropicalAndSubtropicalDryBroadleafForest           named biome2 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TemperateBroadleafAndMixedForest                   named biome4 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TemperateConiferForest                             named biome5 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of BorealForest                                       named biome6 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TropicalAndSubtropicalGrasslandSavannaAndShrubland named biome7 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of TemperateGrasslandSavannaAndShrubland              named biome8 ,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of MontaneGrasslandAndShrubland                       named biome10,
//		@predictor(discretization = weka.discretizer.unsupervised(bins = 2)) value of MediterraneanForestWoodlandAndScrub                named biome12
	using im.weka.bayesnet(	
    	discretization = weka.discretizer.unsupervised(bins = 3, equalfrequency=true),
    	resource = bn.visits.donana.nobiom, search = weka.bayes.k2(maxparents=2)
    ); 

@documented(observ_documentation.machine_learning)

// WARNING: SOIL-DENSITY and SOIL-ORGANIC-C layers need to be run manually before running the model, otherwise they are not found and the observation is unsuccessful
//@documented(observ_documentation.pollination_score)
@intensive(space)
model local:angel.gimenez:observ.sandbox:bn.visits
//	local:stefano.balbi:observ.sandbox:bn.visits.eu  
//	local:joaopompeu:observ.sandbox:bn.visits.eur 
//	local:joaopompeu:observ.sandbox:bn.visits.donana
//local:joaopompeu:observ.sandbox:bn.visits.donana.nobiom
	as occurrence of PollinatorVisit named machine_learning_visit_score
	observing 
		im:Mean earth:AtmosphericTemperature in Celsius,
		im:Mean TemperatureDiurnalRange in Celsius     ,
		im:Maximum TemperatureWarmestMonth in Celsius  ,
		im:Mean TemperatureWettestQuarter in Celsius   ,
		@extensive(time) PrecipitationVolumeDriestMonth in mm           ,
		geography:Elevation in m                       ,
		soil.incubation:DroughtSeverityIndex           ,
		percentage of landcover:BareArea               ,
		percentage of landcover:ArtificialSurface      ,
		percentage of landcover:Shrubland              ,
		SoilDensity                           ,
		SoilOrganicCarbonContent              ,
		value of TropicalAndSubtropicalMoistBroadleafForest        ,
		value of TropicalAndSubtropicalDryBroadleafForest          ,
		value of TemperateBroadleafAndMixedForest                  ,
		value of TemperateConiferForest                            ,
		value of BorealForest                                      ,
		value of TropicalAndSubtropicalGrasslandSavannaAndShrubland,
		value of TemperateGrasslandSavannaAndShrubland             ,
		value of MontaneGrasslandAndShrubland                      ,
		value of MediterraneanForestWoodlandAndScrub
		set to [Math.exp(self)], klab.data.normalize();


/////////////////////////
//// LOGISTIC REGRESSION
//@intensive(space)
//learn occurrence of PollinatorVisit within earth:Site
//	observing 
//		@archetype earth:Site with occurrence of PollinatorVisit,
//		@predictor im:Mean earth:AtmosphericTemperature in Celsius named annmeant ,
//		@predictor im:Mean TemperatureDiurnalRange in Celsius      named diurange ,
//		@predictor im:Maximum TemperatureWarmestMonth in Celsius   named maxtwarm ,
//		@predictor im:Mean TemperatureWettestQuarter in Celsius    named twetquart,
//		@predictor PrecipitationVolumeDriestMonth in mm            named precdrmo ,
//		@predictor geography:Elevation in m                        named elev     ,
//		@predictor soil.incubation:DroughtSeverityIndex            named pdsi     ,
//		@predictor percentage of landcover:BareArea                named bare     ,
//		@predictor percentage of landcover:ArtificialSurface       named urban    ,
//		@predictor percentage of landcover:Shrubland               named shrub    ,
//		@predictor SoilDensity in kg/m^3                           named soilden  ,
//		@predictor SoilOrganicCarbonContent in g/kg                named soilc    ,
//		@predictor value of TropicalAndSubtropicalMoistBroadleafForest         named biome1 ,
//		@predictor value of TropicalAndSubtropicalDryBroadleafForest           named biome2 ,
//		@predictor value of TemperateBroadleafAndMixedForest                   named biome4 ,
//		@predictor value of TemperateConiferForest                             named biome5 ,
//		@predictor value of BorealForest                                       named biome6 ,
//		@predictor value of TropicalAndSubtropicalGrasslandSavannaAndShrubland named biome7 ,
//		@predictor value of TemperateGrasslandSavannaAndShrubland              named biome8 ,
//		@predictor value of MontaneGrasslandAndShrubland                       named biome10,
//		@predictor value of MediterraneanForestWoodlandAndScrub                named biome12
//	using im.weka.simplelogistic(
//		discretization = weka.discretizer.unsupervised(bins = 4, equalfrequency=true),	
//    	resource = logistic.visits
//    );	        
//
//@intensive(space)
//model local:angel.gimenez:observ.sandbox:logistic.visits
//	as occurrence of PollinatorVisit
//	observing 
//		im:Mean earth:AtmosphericTemperature in Celsius,
//		im:Mean TemperatureDiurnalRange in Celsius     ,
//		im:Maximum TemperatureWarmestMonth in Celsius  ,
//		im:Mean TemperatureWettestQuarter in Celsius   ,
//		PrecipitationVolumeDriestMonth in mm           ,
//		geography:Elevation in m                       ,
//		soil.incubation:DroughtSeverityIndex           ,
//		percentage of landcover:BareArea               ,
//		percentage of landcover:ArtificialSurface      ,
//		percentage of landcover:Shrubland              ,
//		SoilDensity in kg/m^3                          ,
//		SoilOrganicCarbonContent in g/kg               ,
//		value of TropicalAndSubtropicalMoistBroadleafForest        ,
//		value of TropicalAndSubtropicalDryBroadleafForest          ,
//		value of TemperateBroadleafAndMixedForest                  ,
//		value of TemperateConiferForest                            ,
//		value of BorealForest                                      ,
//		value of TropicalAndSubtropicalGrasslandSavannaAndShrubland,
//		value of TemperateGrasslandSavannaAndShrubland             ,
//		value of MontaneGrasslandAndShrubland                      ,
//		value of MediterraneanForestWoodlandAndScrub;                
//	        	        
model im:Net value of ecology:Pollination named pollination_service_ml
	observing
		landcover:LandCoverType                                                                named landcover_type_ml,
		im:Normalized ratio of agriculture:Pollinated agriculture:Yield to agriculture:Yield   named pollinated_yield_ml,
		occurrence of PollinatorVisit named pollinator_visitation_rate_ml
	set to [ (landcover_type_ml is landcover:AgriculturalVegetation) ? (2*pollinator_visitation_rate_ml*pollinated_yield_ml - pollinated_yield_ml*pollinated_yield_ml) : (nodata) ];


 
 
 